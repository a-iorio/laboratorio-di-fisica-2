import pylab
from scipy.optimize import curve_fit
import numpy

z, V, dV = pylab.loadtxt('dati.txt', unpack = True)
dz = pylab.array([0.2]*len(z), 'd')

z2, V2, dV2 = pylab.loadtxt('dati2.txt', unpack = True)
V2 = V2*10**-1
dV2 = dV2*10**-1
dz2 = pylab.array([0.2]*len(z2), 'd')

z3, V3, dV3 = pylab.loadtxt('datic.txt', unpack = True)
dz3 = pylab.array([0.2]*len(z3), 'd')

z4, V4, dV4 = pylab.loadtxt('dati2c.txt', unpack = True)
dz4 = pylab.array([0.2]*len(z4), 'd')
V4 = V4*10**-1
dV4 = dV4*10**-1

grid = numpy.logspace(0.95, 1.4, 100)

def fit(z, b):
	return 155/z**b

def fit2(z, b):
	return 106/z**b

popt, pcov = curve_fit(fit, z, V, pylab.array([3]), dV)
b = popt
db = pylab.sqrt(pcov.diagonal())
chisquare = sum(((V-fit(z, b))/dV)**2)

print ("Parametri stimati: b = %f \pm %f" % (b, db))
print ("Chisquare = %f" % (chisquare))


popt, pcov = curve_fit(fit2, z2, V2, pylab.array([4]), dV2)
b2 = popt
db2 = pylab.sqrt(pcov.diagonal())
chisquare = sum(((V2-fit2(z2, b2))/dV2)**2)

#print ("Parametri stimati: A = %f \pm %f" % (A2, dA2))
print ("Parametri stimati: b = %f \pm %f" % (b2, db2))
print ("Chisquare = %f" % (chisquare))
#print ("Indice di Pearson: c = %f \n" % (pcov[0,1]/(dA2*db2)))

pylab.figure(1)
pylab.title("Plot - Esperienza campo magnetico")
pylab.xlim(3,40)
pylab.xscale('log')
pylab.yscale('log')
pylab.xlabel('z [cm]')
pylab.ylabel('V [V]')
pylab.errorbar(z3, V3, dV3, dz3, '+')
pylab.errorbar(z4, V4, dV4, dz4, '+')
pylab.plot(grid, fit(grid, b), color = 'black')
pylab.plot(grid, fit2(grid, b2), color = 'black')

pylab.grid()
pylab.savefig('plot1.pdf')
pylab.show()