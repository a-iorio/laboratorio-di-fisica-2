import pylab
import numpy
from scipy.optimize import curve_fit

r, dr, i, di = pylab.loadtxt('dati.txt', unpack = True)

#modifico unita misura
i = i/10**3
di= di/10**3
v0=5.06

pylab.figure(1)
pylab.title('Legge di Ohm')
pylab.subplot(211)
pylab.ylabel('I [A]')
pylab.grid()
pylab.errorbar(r, i, xerr=dr, yerr=di, fmt='+', color='black')
pylab.xscale('log')
pylab.yscale('log')
grid = numpy.logspace(1,6,100)

def f(r, r_int):
	return (v0/(r+r_int))

popt, pcov = curve_fit(f, r, i, pylab.array([2]), di)
r_int = popt
dr_int = pylab.sqrt(pcov.diagonal())
print r_int
print dr_int
chisquare = sum(((i-f(r,r_int))/di)**2)
print chisquare

pylab.plot(grid, f(grid, 23), color='black')

pylab.subplot(212)
pylab.grid()
pylab.xlabel('R [ohm]')
pylab.ylabel('I dati-modello [A]')
pylab.xscale('log')
pylab.yscale('log')
pylab.plot(r, (i-f(r,r_int))/i, 'o')
pylab.savefig('plot.pdf')
pylab.show()