import pylab
from scipy.optimize import curve_fit
import numpy

R = 677
dR = 5
r = 40.9
dr = 0.3
C = 0.1e-6
dC = 0.1*C
L = 0.5
f0 = 1/(2*numpy.pi*pylab.sqrt(L*C))
df0 = 0.7e2

f, df, v_out, dv_out, v_in, dv_in = pylab.loadtxt('data.txt', unpack = True)
A = v_out/v_in
dA = (dv_out/v_out+dv_in/v_in)*A

c1_r = 2*numpy.pi*R*C
dc1_r = 0.11*c1_r
c2_r = 2*numpy.pi*(R+r)*C
dc2_r = 0.11*c2_r
c3_r = f0
dc3_r = df0
print("Parametri attesi: \nc1 = %f +- %f, c2 = %f +- %f, c3 = %f +- %f \n" % (c1_r, dc1_r, c2_r, dc2_r, c3_r, dc3_r))
grid = numpy.linspace(1,2000,1000)


def fit(f, c1, c2, c3):
	return c1*f/(pylab.sqrt((c2*f)**2+(1-(f/c3)**2)**2))
popt, pcov = curve_fit(fit, f, A, pylab.array([c1_r, c2_r, c3_r]), dA)
c1, c2, c3  = popt
dc1, dc2, dc3 = pylab.sqrt(pcov.diagonal())
f_fwhm = pylab.sqrt(3)*c2*c3**2
df_fwhm = pylab.sqrt(3)*pylab.sqrt((c2*2*c3*dc3)**2+(dc2*c3**2)**2)
chisquare = sum(((A-fit(f, c1, c2, c3))/dA)**2)
print ("DATI FIT #1\n")
print ("Chi quadro: %.1f per un sistema a %.f-3 gradi di liberta" % (chisquare, len(A)))
print ("Parametri stimati: c1 = %f +- %f, c2 = %f +- %f, c3 = %f +- %f" % (c1, dc1, c2, dc2, c3, dc3))
print ("Indice di Pearson: c12 = %f, c13 = %f, c23 = %f" % (pcov[0, 1]/(dc1*dc2), pcov[0, 2]/(dc1*dc3), pcov[1, 2]/(dc2*dc3)))
print ("f_fwhm = %.f +- %.f \n" % (f_fwhm, df_fwhm))

pylab.figure(1)
pylab.title('Plot risonanza')
pylab.grid()
pylab.errorbar(f, A, dA, df, 'o')
pylab.plot(grid, fit(grid, c1, c2, c3), label='Fit (c1, c2, c3)', linewidth=1.5)
pylab.legend()
pylab.ylabel('A(f)')
pylab.xlabel('f [Hz]')


def fit(f, c1, c3):
	return c1*f/(pylab.sqrt((c1*f)**2+(1-(f/c3)**2)**2))
popt, pcov = curve_fit(fit, f, A, pylab.array([c1_r, c3_r]), dA)
c1, c3  = popt
dc1, dc3 = pylab.sqrt(pcov.diagonal())
f_fwhm = pylab.sqrt(3)*c1*c3**2
df_fwhm = pylab.sqrt(3)*pylab.sqrt((c1*2*c3*dc3)**2+(dc1*c3**2)**2)
chisquare = sum(((A-fit(f, c1, c3))/dA)**2)
print ("DATI FIT #2\n")
print ("Chi quadro: %.1f per un sistema a %.f-2 gradi di liberta" % (chisquare, len(A)))
print ("Parametri stimati: c1 = %f +- %f, c3 = %f +- %f" % (c1, dc1, c3, dc3))
print ("Indice di Pearson: c13 = %f" % (pcov[0,1]/(dc1*dc3)))
print ("f_fwhm = %.f +- %.f \n" % (f_fwhm, df_fwhm))

pylab.figure(1)
pylab.plot(grid, fit(grid, c1, c3), label='Fit (c1, c3)', linewidth=1.5, linestyle=':')
pylab.legend()

def fit(f, c1, c2):
	return c1*f/(pylab.sqrt((c2*f)**2+(1-(f/f0)**2)**2))
popt, pcov = curve_fit(fit, f, A, pylab.array([c1_r, c2_r]), dA)
c1, c2 = popt
dc1, dc2 = pylab.sqrt(pcov.diagonal())
chisquare = sum(((A-fit(f, c1, c2))/dA)**2)
print ("DATI FIT #3\n")
print ("Chi quadro: %.1f per un sistema a %.f-2 gradi di liberta" % (chisquare, len(A)))
print ("Parametri stimati: c1 = %f +- %f, c2 = %f +- %f" % (c1, dc1, c2, dc2))
print ("Indice di Pearson: c12 = %f \n" % (pcov[0,1]/(dc1*dc2)))

pylab.figure(1)
pylab.plot(grid, fit(grid, c1, c2), label='Fit (c1, c2)', linewidth=1.5, linestyle='--')
pylab.legend()

pylab.savefig('risonanza.pdf')
pylab.show()