import pylab
from scipy.optimize import curve_fit
import numpy

V0 = 1023

def fit_C(t, tau):
	return V0*(1-numpy.exp(-t/tau))

def fit_S(t, tau):
	return V0*numpy.exp(-t/tau)

grid = numpy.linspace(0, 25000, 1000)

###########################################################################################

pylab.figure(1)
pylab.title("Carica e scarica di un condensatore")
pylab.grid()
pylab.xlim(-1,25)
pylab.xlabel("t [ms]")
pylab.ylabel("V(t) [arb. un.]")

###########################################################################################


t, V = pylab.loadtxt('ioriotirone_C.txt', unpack = True)
yerr = pylab.array(len(V)*[1], 'd')
xerr = pylab.array(len(V)*[0.1], 'd')

popt, pcov = curve_fit(fit_C, t, V, pylab.array([3300]), yerr)
tau = popt
dtau = pylab.sqrt(pcov.diagonal())

yerr_n = numpy.sqrt(yerr**2+(((numpy.exp(-(t)/tau))/tau)*xerr*1000)**2)

popt, pcov = curve_fit(fit_C, t, V, pylab.array([3300]), yerr_n)
tau = popt
dtau = pylab.sqrt(pcov.diagonal())
print ("Parametri stimati: tau = %f \pm %f" % (tau/1000, dtau/1000))

chisquare = sum(((V-fit_C(t, tau))/yerr_n)**2)

print ("DATI FIT C \n")
print ("Chisquare = %f" % (chisquare))
print ("Ndof = %.f-1" % (len(V)))
print ("Parametri stimati: tau = %f \pm %f \n" % (tau/1000, dtau/1000))

pylab.errorbar(t/1000, V, yerr, xerr, '+')
pylab.plot(grid/1000, fit_C(grid, tau))

###########################################################################################

t, V = pylab.loadtxt('ioriotirone_S.txt', unpack = True)
yerr = pylab.array(len(V)*[1], 'd')
xerr = pylab.array(len(V)*[0.1], 'd')

popt, pcov = curve_fit(fit_S, t, V, pylab.array([3300]), yerr)
tau = popt
dtau = pylab.sqrt(pcov.diagonal())
print ("Parametri stimati: tau = %f \pm %f" % (tau/1000, dtau/1000))

yerr_n = numpy.sqrt(yerr**2+(((numpy.exp(-(t)/tau))/tau)*xerr*1000)**2)

popt, pcov = curve_fit(fit_S, t, V, pylab.array([3300]), yerr_n)
tau = popt
dtau = pylab.sqrt(pcov.diagonal())

chisquare = sum(((V-fit_S(t, tau))/yerr_n)**2)

print ("DATI FIT S \n")
print ("Chisquare = %f" % (chisquare))
print ("Ndof = %.f-1" % (len(V)))
print ("Parametri stimati: tau = %f \pm %f" % (tau/1000, dtau/1000))

pylab.errorbar(t/1000, V, yerr, xerr, '+')
pylab.plot(grid/1000, fit_S(grid, tau))

pylab.savefig('plot_C.pdf')
pylab.show()