import pylab
from scipy.optimize import curve_fit
import numpy

v1,v2 = pylab.loadtxt('ioriotironediodo.txt', unpack = True)
xi = 4.82e-3
dxi = 0.03e-3
v2 = xi*v2
v1 = xi*v1
dv2 = 0.8/100*v2


vref = 4.94
dvref = 0.04
Rd = 3.28e3
dRd = 0.03e3

dv2v1 = numpy.sqrt((2*xi)**2+dvref**2)
dv2 = numpy.sqrt(xi**2+dv2**2)
I = (v1-v2)/Rd*1000
dI = numpy.sqrt((dv2v1/Rd)**2 + (dRd/(Rd**2)*(v1-v2))**2)*1000


grid = numpy.linspace(0,0.615,100)

def fit(V, a, b):
	return a*(numpy.exp(V/b)-1)

popt, pcov = curve_fit(fit, v2, I, pylab.array([10e-6, 50e-3]), dI)
a, b  = popt
da, db = pylab.sqrt(pcov.diagonal())

chisquare = sum(((I-fit(v2, a, b))/dI)**2)

print ("DATI FIT \n")
print ("Chi quadro: %.1f per un sistema a %.f-2 gradi di liberta" % (chisquare, len(v2)))
print ("Parametri stimati: I0 = %f +- %f, nV = %f +- %f" % (a, da, b, db))
print ("Indice di Pearson: ab = %f" % (pcov[0,1]/(da*db)))


dI_n = numpy.sqrt(dI**2+((v2/b)*numpy.exp(v2/b)*dv2*a)**2)

popt, pcov = curve_fit(fit, v2, I, pylab.array([10e-6, 50e-3]), dI_n)
a, b  = popt
da, db = pylab.sqrt(pcov.diagonal())

chisquare = sum(((I-fit(v2, a, b))/dI_n)**2)

print ("DATI FIT 2 \n")
print ("Chi quadro: %.1f per un sistema a %.f-2 gradi di liberta" % (chisquare, len(v2)))
print ("Parametri stimati: I0 = %f +- %f, nV = %f +- %f" % (a, da, b, db))
print ("Indice di Pearson: ab = %f" % (pcov[0,1]/(da*db)))



pylab.figure(1)
pylab.plot(grid, fit(grid, a, b))
pylab.title("Curva caratteristica di un diodo")
pylab.xlabel('V2 [V]')
pylab.ylabel('I [mA]')
pylab.errorbar(v2,I, dI, dv2, '+')
pylab.grid()
pylab.savefig('diodo.pdf')
pylab.show()