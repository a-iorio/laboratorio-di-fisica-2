import pylab
from scipy.optimize import curve_fit
import numpy

R = 5
dR = 5
r = 40.9
dr = 0.3
C = 0.1e-6
dC = 0.1*C
L = 0.5
f0 = 1/(2*numpy.pi*pylab.sqrt(L*C))
df0 = 0.7e2

f, df, v_out, dv_out, v_in, dv_in = pylab.loadtxt('data.txt', unpack = True)
A = v_out/v_in
dA = (dv_out/v_out+dv_in/v_in)*A

Adb = 20*numpy.log10(A)
dAdb = (20/numpy.log(10))*(dA/A)

grid = numpy.linspace(1,2000,1000)
func_grid = numpy.logspace(1, 4, 1000)



pylab.figure(1)
pylab.title('Diagramma di Bode per diversi $Q_f$')
pylab.grid()
pylab.ylabel('A [dB]')
pylab.xlabel('f [Hz]')
pylab.xscale('log')
pylab.ylim(-100, 0)
def fit(f, R):
	return 20*numpy.log10(2*numpy.pi*R*C*f/(pylab.sqrt((2*numpy.pi*f*C*(R)))**2+(1-(f/f0)**2)**2))
Q1=numpy.sqrt(L/C)/22.3
Q2=numpy.sqrt(L/C)/(220/5)
Q3=numpy.sqrt(L/C)/220
Q4=numpy.sqrt(L/C)/(2000/5)
Q5=numpy.sqrt(L/C)/10000


pylab.plot(func_grid, fit(func_grid, 22.3), label='Q = %d' % Q1)
pylab.plot(func_grid, fit(func_grid, 220/5), label='Q = %d' % Q2)
pylab.plot(func_grid, fit(func_grid, 220), label='Q = %d' % Q3)
pylab.plot(func_grid, fit(func_grid, 2000/5), label='Q = %d' % Q4)
pylab.plot(func_grid, fit(func_grid, 10000), label='Q = %d' % Q5)
dbline2=func_grid*0-3
pylab.plot(func_grid, dbline2, '--')

pylab.legend(prop={'size':10})

pylab.savefig('f2.pdf')
pylab.show()