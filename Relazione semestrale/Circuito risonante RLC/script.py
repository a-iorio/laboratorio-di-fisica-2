import pylab
from scipy.optimize import curve_fit
import numpy

R = 677
dR = 5
r = 40.9
dr = 0.3
C = 0.1e-6
dC = 0.1*C
L = 0.5
f0 = 1/(2*numpy.pi*pylab.sqrt(L*C))
df0 = 0.7e2

f, df, v_out, dv_out, v_in, dv_in = pylab.loadtxt('data.txt', unpack = True)
A = v_out/v_in
dA = (dv_out/v_out+dv_in/v_in)*A

Adb = 20*numpy.log10(A)
dAdb = (20/numpy.log(10))*(dA/A)

c1_r = 2*numpy.pi*R*C
dc1_r = 0.11*c1_r
c2_r = 2*numpy.pi*(R+r)*C
dc2_r = 0.11*c2_r
c3_r = f0
dc3_r = df0
print("Parametri attesi: \nc1 = %f +- %f, c2 = %f +- %f, c3 = %f +- %f \n" % (c1_r, dc1_r, c2_r, dc2_r, c3_r, dc3_r))
grid = numpy.linspace(1,2000,1000)
func_grid = numpy.logspace(2, 4, 100)



pylab.figure(1)
pylab.title('Diagramma di Bode - Filtro passa-banda RLC')
pylab.grid()
pylab.errorbar(f, Adb, dAdb, df, 'o')
pylab.ylabel('A [dB]')
pylab.xlabel('f [Hz]')
pylab.xscale('log')

def fit(f, c1, c3):
	return 20*numpy.log10(c1*f/(pylab.sqrt((c1*f)**2+(1-(f/c3)**2)**2)))

popt, pcov = curve_fit(fit, f, Adb, pylab.array([c1_r, c3_r]), dAdb)
c1, c3  = popt
dc1, dc3 = pylab.sqrt(pcov.diagonal())
chisquare = sum(((Adb-fit(f, c1, c3))/dAdb)**2)

print ("DATI FIT \n")
print ("Chi quadro: %.1f per un sistema a %.f-2 gradi di liberta" % (chisquare, len(A)))
print ("Parametri stimati: c1 = %f +- %f, c3 = %f +- %f" % (c1, dc1, c3, dc3))
print ("Indice di Pearson: c13 = %f" % (pcov[0,1]/(dc1*dc3)))
dbline=func_grid*0-6
dbline2=func_grid*0-3
pylab.figure(1)
pylab.plot(func_grid, fit(func_grid, c1, c3))

pylab.plot(func_grid,dbline, '--')
pylab.plot(func_grid,dbline2, '--')


pylab.savefig('boderlc.pdf')
pylab.show()