import pylab
from scipy.optimize import curve_fit
import numpy

R = 1
dR = 5
r = 0
dr = 0.3
C = 1
dC = 0.1*C
L = 1
f0 = 1/(2*numpy.pi*pylab.sqrt(L*C))
df0 = 0.7e2

omega_0 = 2*numpy.pi*f0
func_grid = numpy.logspace(-6, 6, 100)
omegagrid = numpy.linspace(0,10,1000)
pylab.figure(1)
pylab.title('Diagramma di Nyquist - Filtro passa-banda RLC')
pylab.grid()
pylab.xlabel('Re{A($\omega$)}')
pylab.ylabel('Im{A($\omega$)}')

def fit(f, R, r, C):
	return 20*numpy.log10(2*numpy.pi*R*C*f/(pylab.sqrt((2*numpy.pi*f*C*(R+r)))**2+(1-(f/f0)**2)**2))

def f1(omega):
	return omega*(R+r)*C

def f2(omega):
	return 1-(omega/omega_0)**2

dbline=func_grid*0-6
dbline2=func_grid*0-3

pylab.plot(f1(omegagrid), f2(omegagrid))

pylab.savefig('Nyquist.pdf')
pylab.show()